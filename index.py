from flask import Flask, request
from netaddr import *
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Flask Dockerized by Muhammet Arslan'

@app.route('/subnet/')
def show_first_and_last_ip():
    ## That will calculate the subnet things
   	
   	## Getting the CIDR
    cidr = request.args.get('cidr')
    
    ip = IPSet([cidr])
    return str(ip.iprange())

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')